# TP1

Réalisez un jeu dans le style de wordle. Il doit permettre de saisir un mot au joueur tant que ce dernier est en vie. Après chaque saisie, le programme donne au joueur son nombre de vies restantes et les lettres trouvées. Sinon, il s'arrête.
Les vies doivent être représentées par un nombre de ♥ (\u2665) dans une String.

Voici un exemple :

````
Vous devez trouver _ _ _ _
PV : ♥♥♥♥
Entrez votre proposition : liste
li _ _
PV : ♥♥♥
Entrez votre proposition : lien
li _ n
PV : ♥♥
Entrez votre proposition : lion
lion
C'est gagné !

````

Codé en Java, voici ce que celà donne :
````Java
public class Main {

    private static final String ECHAPEMENT = "_";

    private static String verifieChaqueCaractereEtConcatener(String motATrouver, String motSaisi) {
        String retour = "";
        for (int i=0;i<motATrouver.length();i++){
            retour += prendSiIdentique(motATrouver.toLowerCase().charAt(i),motSaisi.toLowerCase().charAt(i))
                        ?motSaisi.charAt(i)
                        :ECHAPEMENT;
        }
        return retour;
    }

    private static boolean prendSiIdentique(char aTrouver, char proposition) {
        return aTrouver == proposition;
    }

    public static void main(String[] args) {
        final String MOT_A_TROUVER = "Lion";

        var saisie = new Scanner(System.in);
        var vie = "♥♥♥♥";

        System.out.println("Vous devez trouver ____");
        while (!vie.isEmpty()) {
            System.out.println("PV : "+vie);
            System.out.print("Entrez votre proposition :");
            String motSaisi = saisie.nextLine();
            String resultat = verifieChaqueCaractereEtConcatener(MOT_A_TROUVER,motSaisi);
            System.out.println(resultat);
            if (resultat.contains(ECHAPEMENT)) {
                vie = vie.substring(0,vie.length()-1);
            } else {
                break;
            }
        }
        if(vie.isEmpty()){
            System.out.println("C'est perdu");
        } else {
            System.out.println("C'est gagné");
        }

    }
}
````

### Q1 : Traduisez en Kotlin le code Java ci-dessous.

### Q2 : Transformez votre code pour le rendre plus "kotlin spirit". Par exemple, vous pourriez créer l'opérateur -- sur String.

### Q3 : Essayez de réduire au maximum le nombre de lignes de votre code.

### Q4 : Ajoutez la gestion des lettres mal placées avec une coloration différente pour les signaler.